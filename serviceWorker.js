const staticPinBall = "dev-coffee-site-v1"
const assets = [
  "/",
  "/index.html",
  "/script.js",
  "/style.css",
  "/maskable_icon_x48.jpg",
  "/maskable_icon_x72.jpg",
  "/maskable_icon_x96.jpg",
  "/maskable_icon_x128.jpg",
  "/maskable_icon_x192.jpg",
  "/maskable_icon_x384.jpg",
  "/maskable_icon_x512.jpg",
]

self.addEventListener("install", installEvent => {
    installEvent.waitUntil(
        caches.open(staticPinBall).then(cache => {
            cache.addAll(assets)
        })
    )
})

self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
        caches.match(fetchEvent.request).then(res => {
            return res || fetch(fetchEvent.request)
        })
    )
})

if ("serviceWorker" in navigator) {
    window.addEventListener("load", function () {
        navigator.serviceWorker
            .register("/serviceWorker.js")
            .then(res => console.log("service worker registered"))
            .catch(err => console.log("service worker not registered", err))
    })
}